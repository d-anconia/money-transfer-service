package com.revolut.task.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@NoArgsConstructor
public class Account {

    @Getter
    private Long id;

    @Getter
    private AtomicReference<BigDecimal> balance;

    @Getter
    @JsonIgnore
    private ReadWriteLock lock;

    public Account(Long id, BigDecimal balance) {
        this.id = id;
        this.balance = new AtomicReference<>(balance.setScale(2, RoundingMode.HALF_EVEN));
        this.lock = new ReentrantReadWriteLock();
    }
}

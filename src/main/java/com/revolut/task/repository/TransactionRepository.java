package com.revolut.task.repository;

import com.google.inject.Singleton;
import com.revolut.task.dto.TransactionDto;
import com.revolut.task.model.Transaction;

import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Singleton
public class TransactionRepository {

    private final Map<Long, Transaction> transactionStorage = new ConcurrentHashMap<>();
    private AtomicLong transactionId = new AtomicLong(0);

    public Transaction saveTransaction(TransactionDto transactionDto) {
        Long newTransactionId = transactionId.incrementAndGet();

        Transaction transaction = Transaction.builder()
                .id(newTransactionId)
                .senderAccountId(transactionDto.getSenderAccountId())
                .recipientAccountId(transactionDto.getRecipientAccountId())
                .amount(transactionDto.getAmount().setScale(2, RoundingMode.HALF_EVEN))
                .transactionTime(LocalDateTime.now())
                .build();

        transactionStorage.put(newTransactionId, transaction);
        return transaction;
    }

    public Optional<Transaction> findTransactionById(Long id) {
        return Optional.ofNullable(transactionStorage.get(id));
    }

    public List<Transaction> getAllTransactionsByUserId(Long userId) {
        return transactionStorage.values()
                .stream()
                .filter(transaction -> transaction.getSenderAccountId().equals(userId)
                        || transaction.getRecipientAccountId().equals(userId))
                .collect(Collectors.toList());
    }

    // top transactions

}

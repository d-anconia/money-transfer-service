package com.revolut.task.validator;

import com.revolut.task.dto.TransactionDto;
import com.revolut.task.exception.TransactionValidationException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class TransactionValidatorTest {

    private final TransactionValidator validator = new TransactionValidator();
    private TransactionDto transactionDto;

    @Before
    public void init() {
        transactionDto = TransactionDto.builder()
                .senderAccountId(1L)
                .recipientAccountId(2L)
                .amount(BigDecimal.TEN)
                .build();
    }

    @Test
    public void validate() throws TransactionValidationException {
        validator.validate(transactionDto);
    }

    @Test(expected = TransactionValidationException.class)
    public void validateTransactionIsNull() throws TransactionValidationException {
        validator.validate(null);
    }

    @Test(expected = TransactionValidationException.class)
    public void validateSameAccounts() throws TransactionValidationException {
        transactionDto.setRecipientAccountId(1L);
        transactionDto.setSenderAccountId(1L);
        validator.validate(transactionDto);
    }

    @Test(expected = TransactionValidationException.class)
    public void validateAmount() throws TransactionValidationException {
        transactionDto.setAmount(BigDecimal.ZERO);
        validator.validate(transactionDto);
    }
}
package com.revolut.task.repository;

import com.revolut.task.dto.TransactionDto;
import com.revolut.task.model.Transaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class TransactionRepositoryTest {

    private final TransactionRepository transactionRepository = new TransactionRepository();
    private Transaction transaction;

    @Before
    public void init() {
        TransactionDto transactionDto = TransactionDto.builder()
                .recipientAccountId(1L)
                .senderAccountId(2L)
                .amount(BigDecimal.TEN)
                .build();
        transaction = transactionRepository.saveTransaction(transactionDto);
    }

    @Test
    public void saveTransaction() {
        Assert.assertNotNull(transaction);
    }

    @Test
    public void findTransactionById() {
        Optional<Transaction> foundTransaction = transactionRepository.findTransactionById(transaction.getId());
        Assert.assertTrue(foundTransaction.isPresent());

        Optional<Transaction> notFoundTransaction = transactionRepository.findTransactionById(999L);
        Assert.assertFalse(notFoundTransaction.isPresent());
    }

    @Test
    public void getAllTransactionsByUserId() {
        List<Transaction> allTransaction = transactionRepository.getAllTransactionsByUserId(2L);
        Assert.assertFalse(allTransaction.isEmpty());
    }
}
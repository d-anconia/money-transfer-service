# Money-transfer-service

## Technology stack

* Java 8 
* Jooby
* Lombok
* Logback
* JUnit
* Mockito

## Endpoints:

#### POST   /user
* HTTP Request:
```json
{
	"firstName" : "Ivan",
	"middleName" : "Ivanov",
	"lastName" : "Petrovich",
	"balance": "200.22"
}
```
* HTTP Response: 201 CREATED
  
#### GET    /user/{userId}    
* HTTP Request: /user/1
* HTTP Response:
```json
{
    "id": 1,
    "firstName": "Ivan",
    "middleName": "Ivanov",
    "lastName": "Petrovich",
    "accounts": [
        {
            "id": 1,
            "balance": 200.22
        }
    ]
}
```
#### GET    /user/{userId}/transaction
* HTTP Request:/user/1/transaction
* HTTP Response:
```json
[
    {
        "id": 1,
        "senderAccountId": 1,
        "recipientAccountId": 2,
        "amount": 10,
        "transactionTime": "2018-11-30 12:00:11"
    }
]
```   
#### POST   /user/{userId}/account
* HTTP Request: user/1/account
```json
{
  "balance": 200
}
```   
* HTTP Response: 201 CREATED

#### DELETE /user/{userId}
* HTTP Request: user/1
* HTTP Response: 200 OK

#### POST   /transfer
* HTTP Request:
```json
{
	"senderAccountId" : 1,
	"recipientAccountId" : 2,
	"amount" : 10
}
```
* HTTP Response: 200 OK

#### GET    /account/{accountId}  
* HTTP Request:/account/1 
* HTTP Response:
```json
{
    "id": 1,
    "balance": 190.22
}
```   
package com.revolut.task.controller;

import com.google.inject.Inject;
import com.revolut.task.dto.AccountDto;
import com.revolut.task.dto.UserDto;
import com.revolut.task.exception.UserNotFoundException;
import com.revolut.task.model.Transaction;
import com.revolut.task.model.User;
import com.revolut.task.service.AccountService;
import com.revolut.task.service.UserService;
import io.jooby.annotations.PathParam;
import org.jooby.Result;
import org.jooby.Status;
import org.jooby.mvc.*;

import java.util.List;

import static com.revolut.task.exception.ExceptionMessage.USER_NOT_FOUND;

@Path("/user")
public class UserController {

    private final UserService userService;
    private final AccountService accountService;

    @Inject
    public UserController(final UserService userService,
                          final AccountService accountService) {
        this.userService = userService;
        this.accountService = accountService;
    }

    @POST
    @Consumes("application/json")
    public Result createUser(@Body UserDto userDto) {
        userService.createUser(userDto);
        return new Result().status(Status.CREATED);
    }

    @GET
    @Path("/{userId}")
    @Produces("application/json")
    public User getUserById(@PathParam Long userId) throws UserNotFoundException {
        return userService.findUserById(userId).orElseThrow(() -> new UserNotFoundException(USER_NOT_FOUND));
    }

    @GET
    @Path("/{userId}/transaction")
    @Produces("application/json")
    public List<Transaction> getAllTransactionsByUserId(@PathParam Long userId) throws UserNotFoundException {
        return userService.getAllTransactionsByUserId(userId);
    }

    @POST
    @Path("/{userId}/account")
    @Consumes("application/json")
    public Result createAccount(@PathParam Long userId, @Body AccountDto accountDto) throws UserNotFoundException {
        accountService.createAccount(userId, accountDto);
        return new Result().status(Status.CREATED);
    }

    @DELETE
    @Path("/{userId}")
    public Result deleteUser(@PathParam Long userId) {
        userService.deleteUser(userId);
        return new Result().status(Status.OK);
    }
}

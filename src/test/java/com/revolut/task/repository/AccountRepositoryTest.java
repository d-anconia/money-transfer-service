package com.revolut.task.repository;

import com.revolut.task.model.Account;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

public class AccountRepositoryTest {

    private final AccountRepository accountRepository = new AccountRepository();

    @Test
    public void createAccount() {
        BigDecimal balance = new BigDecimal(100.12).setScale(2, RoundingMode.HALF_EVEN);
        Account account = accountRepository.createAccount(balance.setScale(2, RoundingMode.HALF_EVEN));
        Assert.assertNotNull(account);
        Assert.assertEquals(balance, account.getBalance().get());

    }

    @Test
    public void findAccountById() {
        BigDecimal balance = new BigDecimal(100.12);
        Account account = accountRepository.createAccount(balance);
        Optional<Account> foundAccount = accountRepository.findAccountById(account.getId());
        Assert.assertTrue(foundAccount.isPresent());
        Assert.assertEquals(account.getId(), foundAccount.get().getId());

        Optional<Account> notFoundAccount = accountRepository.findAccountById(999L);
        Assert.assertFalse(notFoundAccount.isPresent());

    }
}
package com.revolut.task.repository;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.task.dto.UserDto;
import com.revolut.task.model.User;
import lombok.extern.slf4j.Slf4j;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Singleton
@Slf4j
public class UserRepository {

    private final AccountRepository accountRepository;
    private final Map<Long, User> usersStorage = new ConcurrentHashMap<>();
    private final AtomicLong userId = new AtomicLong(0);

    @Inject
    public UserRepository(final AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public User createUser(UserDto userDto) {
        Long newUserId = userId.incrementAndGet();
        User user = User.builder()
                .id(newUserId)
                .firstName(userDto.getFirstName())
                .middleName(userDto.getMiddleName())
                .lastName(userDto.getLastName())
                .accounts(new ArrayList<>(Collections.singletonList(accountRepository.createAccount(userDto.getBalance().setScale(2, RoundingMode.HALF_EVEN)))))
                .build();
        usersStorage.put(newUserId, user);
        log.info("New user with id = {} is created", user.getId());
        return user;
    }

    public void deleteUser(Long userId) {
        log.info("User with id = {} will be deleted", userId);
        usersStorage.remove(userId);
        log.info("User with id = {} is deleted", userId);
    }


    public Optional<User> findUserById(Long id) {
        return Optional.ofNullable(usersStorage.get(id));
    }


}

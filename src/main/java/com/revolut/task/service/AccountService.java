package com.revolut.task.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.task.dto.AccountDto;
import com.revolut.task.exception.AccountNotFoundException;
import com.revolut.task.exception.UserNotFoundException;
import com.revolut.task.model.Account;
import com.revolut.task.model.User;
import com.revolut.task.repository.AccountRepository;
import com.revolut.task.repository.UserRepository;

import static com.revolut.task.exception.ExceptionMessage.ACCOUNT_NOT_FOUND;
import static com.revolut.task.exception.ExceptionMessage.USER_NOT_FOUND;

@Singleton
public class AccountService {

    private final AccountRepository accountRepository;
    private final UserRepository userRepository;

    @Inject
    public AccountService(final AccountRepository accountRepository,
                          final UserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    public Account getAccountById(Long accountId) throws AccountNotFoundException {
        return accountRepository.findAccountById(accountId).orElseThrow(() -> new AccountNotFoundException(ACCOUNT_NOT_FOUND));
    }

    public Account createAccount(Long userId, AccountDto accountDto) throws UserNotFoundException {
        User user = userRepository.findUserById(userId).orElseThrow(() -> new UserNotFoundException(USER_NOT_FOUND));
        Account account = accountRepository.createAccount(accountDto.getBalance());
        user.getAccounts().add(account);
        return account;
    }
}

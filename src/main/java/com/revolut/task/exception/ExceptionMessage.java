package com.revolut.task.exception;

public class ExceptionMessage {
    public static final String TRANSACTION_IS_NULL = "There is no transfer";
    public static final String SAME_ACCOUNTS_EXCEPTION = "Recipient and Sender are the same person";
    public static final String AMOUNT_OF_TRANSFER_IS_NOT_VALID = "Sorry, but we can not send such amount";

    public static final String IMPOSSIBLE_TO_LOCK_ACCOUNT = "Impossible to lock account";
    public static final String NOT_ENOUGH_MONEY = "Sender's balance is less than transfer amount";
    public static final String ACCOUNT_NOT_FOUND = "Account not found";
    public static final String USER_NOT_FOUND = "User not found";
    public static final String TRANSACTION_VALIDATION_MESSAGE = "Transaction is not valid";
    public static final String TRANSACTION_FAILED_MESSAGE = "There were problems during the operation. Try later";
}

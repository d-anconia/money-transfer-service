package com.revolut.task.controller;

import com.google.inject.Inject;
import com.revolut.task.dto.TransactionDto;
import com.revolut.task.exception.AccountNotFoundException;
import com.revolut.task.exception.TransactionFailedException;
import com.revolut.task.exception.TransactionValidationException;
import com.revolut.task.service.TransactionService;
import org.jooby.Result;
import org.jooby.Status;
import org.jooby.mvc.Body;
import org.jooby.mvc.POST;
import org.jooby.mvc.Path;

@Path("/transfer")
public class TransactionController {

    private final TransactionService transactionService;

    @Inject
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @POST
    public Result transfer(@Body TransactionDto transactionDto) throws TransactionValidationException, AccountNotFoundException, TransactionFailedException, InterruptedException {
        transactionService.transfer(transactionDto);
        return new Result().status(Status.OK);
    }
}

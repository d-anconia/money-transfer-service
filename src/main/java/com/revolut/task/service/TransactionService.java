package com.revolut.task.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.task.dto.TransactionDto;
import com.revolut.task.exception.AccountNotFoundException;
import com.revolut.task.exception.TransactionFailedException;
import com.revolut.task.exception.TransactionValidationException;
import com.revolut.task.model.Account;
import com.revolut.task.repository.AccountRepository;
import com.revolut.task.repository.TransactionRepository;
import com.revolut.task.validator.TransactionValidator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.revolut.task.exception.ExceptionMessage.*;

@Singleton
public class TransactionService {

    private final AccountRepository accountRepository;
    private final TransactionValidator validator;
    private final TransactionRepository transactionRepository;

    @Inject
    public TransactionService(final AccountRepository accountRepository,
                              final TransactionValidator validator,
                              final TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.validator = validator;
        this.transactionRepository = transactionRepository;
    }

    public void transfer(TransactionDto transaction) throws TransactionFailedException, TransactionValidationException, AccountNotFoundException, InterruptedException {
        Account sender = accountRepository.findAccountById(transaction.getSenderAccountId()).orElseThrow(() -> new AccountNotFoundException(ACCOUNT_NOT_FOUND));
        Account recipient = accountRepository.findAccountById(transaction.getRecipientAccountId()).orElseThrow(() -> new AccountNotFoundException(ACCOUNT_NOT_FOUND));

        validator.validate(transaction);

        List<Account> accounts = getAccountsOrderedById(sender, recipient);

        tryLockAccounts(accounts);
        try {

            if (transaction.getAmount().compareTo(sender.getBalance().get()) > 0) {
                throw new TransactionFailedException(NOT_ENOUGH_MONEY);
            }

            sender.getBalance().updateAndGet(balance -> balance.subtract(transaction.getAmount()));
            recipient.getBalance().updateAndGet(balance -> balance.add(transaction.getAmount()));
            transactionRepository.saveTransaction(transaction);
        } finally {
            unlockAccounts(accounts);
        }

    }

    /*
     Ordered locking
     */
    private void tryLockAccounts(List<Account> accountList) throws TransactionFailedException, InterruptedException {

        for (Account account : accountList) {
            boolean isAcquired = account.getLock()
                    .writeLock()
                    .tryLock(1, TimeUnit.SECONDS);

            if (!isAcquired) {
                throw new TransactionFailedException(IMPOSSIBLE_TO_LOCK_ACCOUNT);
            }
        }
    }

    private void unlockAccounts(List<Account> accountList) {
        for (Account account : accountList)
            account.getLock().writeLock().unlock();
    }

    private List<Account> getAccountsOrderedById(Account... accounts) {
        return Arrays.stream(accounts)
                .sorted(Comparator.comparingLong(Account::getId))
                .collect(Collectors.toList());
    }
}

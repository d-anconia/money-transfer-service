package com.revolut.task.validator;

import com.google.inject.Singleton;
import com.revolut.task.dto.TransactionDto;
import com.revolut.task.exception.TransactionValidationException;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

import static com.revolut.task.exception.ExceptionMessage.*;

@Singleton
@Slf4j
public class TransactionValidator {

    public void validate(TransactionDto transaction) throws TransactionValidationException {
        if (transaction == null) {
            log.error("Transaction doesn't exist");
            throw new TransactionValidationException(TRANSACTION_IS_NULL);
        }
        if (transaction.getSenderAccountId().equals(transaction.getRecipientAccountId())) {
            log.error("Transaction between same accounts");
            throw new TransactionValidationException(SAME_ACCOUNTS_EXCEPTION);
        }
        if (isAmountTransactionValid(transaction)) {
            log.error("Amount of transaction is invalid");
            throw new TransactionValidationException(AMOUNT_OF_TRANSFER_IS_NOT_VALID);
        }
    }

    private boolean isAmountTransactionValid(TransactionDto transaction) {
        return transaction.getAmount() == null || transaction.getAmount().compareTo(BigDecimal.ZERO) <= 0;
    }
}

package com.revolut.task.service;

import com.revolut.task.dto.TransactionDto;
import com.revolut.task.exception.AccountNotFoundException;
import com.revolut.task.exception.TransactionFailedException;
import com.revolut.task.exception.TransactionValidationException;
import com.revolut.task.model.Account;
import com.revolut.task.repository.AccountRepository;
import com.revolut.task.repository.TransactionRepository;
import com.revolut.task.validator.TransactionValidator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private TransactionRepository transactionRepository;
    @Mock
    private TransactionValidator validator;

    @InjectMocks
    private TransactionService transactionService;

    @Test
    public void transfer() throws InterruptedException, AccountNotFoundException, TransactionFailedException, TransactionValidationException {

        Account sender = new Account(1L, new BigDecimal(2_000).setScale(2, RoundingMode.HALF_EVEN));
        Account recipient = new Account(1L, new BigDecimal(3_000).setScale(2, RoundingMode.HALF_EVEN));

        Mockito.when(accountRepository.findAccountById(1L)).thenReturn(Optional.of(sender));
        Mockito.when(accountRepository.findAccountById(2L)).thenReturn(Optional.of(recipient));
        TransactionDto transactionDto = TransactionDto.builder()
                .senderAccountId(1L)
                .recipientAccountId(2L)
                .amount(BigDecimal.TEN)
                .build();
        transactionService.transfer(transactionDto);
        Assert.assertEquals(new BigDecimal(1_990).setScale(2, RoundingMode.HALF_EVEN), sender.getBalance().get());
        Assert.assertEquals(new BigDecimal(3_010).setScale(2, RoundingMode.HALF_EVEN), recipient.getBalance().get());
    }
}
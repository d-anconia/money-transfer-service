package com.revolut.task;

import com.revolut.task.controller.AccountController;
import com.revolut.task.controller.TransactionController;
import com.revolut.task.controller.UserController;
import com.revolut.task.exception.*;
import org.jooby.Jooby;
import org.jooby.json.Jackson;

import static com.revolut.task.exception.ExceptionMessage.*;

public class App extends Jooby {
    {
        use(new Jackson());
        use(UserController.class);
        use(TransactionController.class);
        use(AccountController.class);

        err(AccountNotFoundException.class, (request, response, error) -> response.send(ExceptionResponse.builder()
                .message(error.getMessage())
                .status(404)
                .description(ACCOUNT_NOT_FOUND)
                .build()));

        err(UserNotFoundException.class, (request, response, error) -> response.send(ExceptionResponse.builder()
                .message(error.getMessage())
                .status(404)
                .description(USER_NOT_FOUND)
                .build()));
        err(TransactionValidationException.class, (request, response, error) -> response.send(ExceptionResponse.builder()
                .message(error.getMessage())
                .status(404)
                .description(TRANSACTION_VALIDATION_MESSAGE)
                .build()));
        err(TransactionFailedException.class, (request, response, error) -> response.send(ExceptionResponse.builder()
                .message(error.getMessage())
                .status(404)
                .description(TRANSACTION_FAILED_MESSAGE)
                .build()));

    }

    public static void main(String[] args) {
        run(App::new, args);
    }
}

package com.revolut.task.controller;


import com.google.inject.Inject;
import com.revolut.task.exception.AccountNotFoundException;
import com.revolut.task.model.Account;
import com.revolut.task.service.AccountService;
import io.jooby.annotations.PathParam;
import org.jooby.mvc.GET;
import org.jooby.mvc.POST;
import org.jooby.mvc.Path;
import org.jooby.mvc.Produces;

@Path("/account")
public class AccountController {

    private final AccountService accountService;

    @Inject
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GET
    @Path("/{accountId}")
    @Produces("application/json")
    public Account getAccount(@PathParam Long accountId) throws AccountNotFoundException {
        return accountService.getAccountById(accountId);
    }
}

package com.revolut.task.repository;

import com.revolut.task.dto.UserDto;
import com.revolut.task.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;

import java.math.BigDecimal;
import java.util.Optional;

public class UserRepositoryTest {

    private final AccountRepository accountRepository = new AccountRepository();
    private final UserRepository userRepository = new UserRepository(accountRepository);
    private User user;

    @Before
    public void init() {
        UserDto userDto = new UserDto("Ivan", "Ivanov", "Petrovich", new BigDecimal(100.32));
        user = userRepository.createUser(userDto);
    }

    @Test
    public void createUser() {
        Assert.assertNotNull(user);
    }

    @Test
    public void findUserById() {
        Optional<User> foundUser = userRepository.findUserById(user.getId());
        Assert.assertTrue(foundUser.isPresent());
        Assert.assertEquals(user.getId(), foundUser.get().getId());
    }

    @Test
    @AfterAll
    public void deleteUser() {
        userRepository.deleteUser(user.getId());
        Assert.assertFalse(userRepository.findUserById(user.getId()).isPresent());
    }
}
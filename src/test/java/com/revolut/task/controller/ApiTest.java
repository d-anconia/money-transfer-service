package com.revolut.task.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.task.App;
import com.revolut.task.dto.TransactionDto;
import com.revolut.task.dto.UserDto;
import com.revolut.task.model.Transaction;
import com.revolut.task.model.User;
import okhttp3.*;
import org.eclipse.jetty.http.HttpStatus;
import org.jooby.test.JoobyRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static io.jooby.MediaType.JSON;

public class ApiTest {

    @Rule
    public JoobyRule boot = new JoobyRule(new App());
    private final ObjectMapper objectMapper = new ObjectMapper();
    private String SERVER_BASE_URL = "http://localhost:8080/";
    private OkHttpClient client = new OkHttpClient();


    @Test
    @Before
    public void createUser() throws IOException {
        UserDto userDto = new UserDto("Ivanov", "Ivan", "Ivanovich", new BigDecimal(100));
        Response response = post("user", objectMapper.writeValueAsString(userDto));
        Assert.assertEquals(HttpStatus.CREATED_201, response.code());
    }

    @Test
    public void getUser() throws IOException {
        Response response = get("user/1");
        Assert.assertEquals(HttpStatus.OK_200, response.code());
    }

    @Test
    public void transfer() throws IOException {
        UserDto userDto = new UserDto("Petrov", "Ivan", "Petrovich", new BigDecimal(1000));
        post("user", objectMapper.writeValueAsString(userDto));
        TransactionDto transactionDto = TransactionDto.builder()
                .senderAccountId(1L)
                .recipientAccountId(2L)
                .amount(BigDecimal.TEN)
                .build();
        post("transfer", objectMapper.writeValueAsString(transactionDto));

        String jsonSender = get("user/1").body().string();
        String jsonRecipient = get("user/2").body().string();

        User sender = objectMapper.readValue(jsonSender, User.class);
        User recipient = objectMapper.readValue(jsonRecipient, User.class);

        Assert.assertEquals("90.00", sender.getAccounts().get(0).getBalance().get().toPlainString());
        Assert.assertEquals("1010.00", recipient.getAccounts().get(0).getBalance().get().toPlainString());

        String jsonTransactions = get("user/1/transaction").body().string();
        List<Transaction> transactions = objectMapper.readValue(jsonTransactions,
                objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, Transaction.class));
        Assert.assertEquals("10.00", transactions.get(0).getAmount().toPlainString());

    }


    private Response post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(MediaType.parse(JSON), json);
        Request request = new Request.Builder()
                .url(SERVER_BASE_URL + url)
                .post(body)
                .build();
        return client.newCall(request).execute();
    }

    private Response get(String url) throws IOException {
        Request request = new Request.Builder()
                .url(SERVER_BASE_URL + url)
                .get()
                .build();
        return client.newCall(request).execute();
    }
}

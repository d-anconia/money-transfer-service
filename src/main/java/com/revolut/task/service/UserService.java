package com.revolut.task.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.task.dto.UserDto;
import com.revolut.task.exception.UserNotFoundException;
import com.revolut.task.model.Transaction;
import com.revolut.task.model.User;
import com.revolut.task.repository.TransactionRepository;
import com.revolut.task.repository.UserRepository;

import java.util.List;
import java.util.Optional;

import static com.revolut.task.exception.ExceptionMessage.USER_NOT_FOUND;

@Singleton
public class UserService {

    private final UserRepository userRepository;
    private final TransactionRepository transactionRepository;

    @Inject
    public UserService(final UserRepository userRepository,
                       final TransactionRepository transactionRepository) {
        this.userRepository = userRepository;
        this.transactionRepository = transactionRepository;
    }

    public User createUser(UserDto userDto) {
        return userRepository.createUser(userDto);
    }


    public void deleteUser(Long userId) {
        userRepository.deleteUser(userId);
    }

    public Optional<User> findUserById(Long id) {
        return userRepository.findUserById(id);
    }

    public List<Transaction> getAllTransactionsByUserId(Long id) throws UserNotFoundException {
        User user = findUserById(id).orElseThrow(() -> new UserNotFoundException(USER_NOT_FOUND));
        return transactionRepository.getAllTransactionsByUserId(user.getId());
    }
}

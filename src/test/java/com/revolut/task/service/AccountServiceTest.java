package com.revolut.task.service;

import com.revolut.task.dto.AccountDto;
import com.revolut.task.exception.AccountNotFoundException;
import com.revolut.task.exception.UserNotFoundException;
import com.revolut.task.model.Account;
import com.revolut.task.model.User;
import com.revolut.task.repository.AccountRepository;
import com.revolut.task.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AccountService accountService;

    @Test
    public void getAccountById() throws AccountNotFoundException {
        Account account = new Account(777L, new BigDecimal(1_000_000));
        Mockito.when(accountRepository.findAccountById(anyLong())).thenReturn(Optional.of(account));
        Account foundAccount = accountService.getAccountById(777L);
        Assert.assertEquals(account.getId(), foundAccount.getId());
    }

    @Test(expected = AccountNotFoundException.class)
    public void getAccountByIdWithException() throws AccountNotFoundException {
        Mockito.when(accountRepository.findAccountById(anyLong())).thenReturn(Optional.empty());
        accountService.getAccountById(777L);
    }

    @Test
    public void createAccount() throws UserNotFoundException {
        Account account = new Account(777L, new BigDecimal(2_000).setScale(2, RoundingMode.HALF_EVEN));
        User user = User.builder()
                .firstName("firstName")
                .middleName("middleName")
                .lastName("lastName")
                .accounts(new ArrayList<>(Collections.singletonList(new Account(1L, new BigDecimal(1_000_000).setScale(2, RoundingMode.HALF_EVEN)))))
                .build();
        Mockito.when(userRepository.findUserById(anyLong())).thenReturn(Optional.of(user));
        Mockito.when(accountRepository.createAccount(any())).thenReturn(account);
        Account newAccount = accountService.createAccount(user.getId(), new AccountDto(new BigDecimal(2_0000).setScale(2, RoundingMode.HALF_EVEN)));

        Assert.assertEquals(account.getBalance().get(), newAccount.getBalance().get());
    }
}
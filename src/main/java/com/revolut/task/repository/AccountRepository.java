package com.revolut.task.repository;


import com.google.inject.Singleton;
import com.revolut.task.model.Account;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Singleton
public class AccountRepository {

    private final Map<Long, Account> accountStorage = new ConcurrentHashMap<>();
    private AtomicLong accountId = new AtomicLong(0);

    public Account createAccount(BigDecimal balance) {
        Long newAccountId = accountId.incrementAndGet();
        Account account = new Account(newAccountId, balance);
        accountStorage.put(newAccountId, account);
        return account;
    }

    public Optional<Account> findAccountById(Long id) {
        return Optional.ofNullable(accountStorage.get(id));
    }

}
